#  File: Queens.py

#  Description: Provides solutions to the queens problem

#  Student Name: Christopher Calizzi

#  Student UT EID: csc3322

#  Course Name: CS 313E

#  Unique Number: 50295

#  Date Created: 3-11-20

#  Date Last Modified: 3-11-20

class Queens(object):
    # initialize the board
    def __init__(self, n=8):
        self.board = []
        self.n = n
        for i in range(self.n):
            row = []
            for j in range(self.n):
                row.append('*')
            self.board.append(row)
        self.solutions = 0

    # print the board
    def print_board(self):
        for i in range(self.n):
            for j in range(self.n):
                print(self.board[i][j], end=' ')
            print()
        print()

    # check if no queen captures another
    def is_valid(self, row, col):
        for i in range(self.n):
            if (self.board[row][i] == 'Q' or self.board[i][col] == 'Q'):
                return False
        for i in range(self.n):
            for j in range(self.n):
                row_diff = abs(row - i)
                col_diff = abs(col - j)
                if (row_diff == col_diff) and (self.board[i][j] == 'Q'):
                    return False
        return True

    # do a recursive backtracking solution
    def recursive_solve(self, col):
        if (col == self.n):
            self.print_board()
            self.solutions += 1
        else:
            for i in range(self.n):
                if (self.is_valid(i, col)):
                    self.board[i][col] = 'Q'
                    self.recursive_solve(col + 1)
                    self.board[i][col] = '*'

    # if the problem has a solution print the board
    def solve(self):
        self.recursive_solve(0)


def main():
    num = 0
    while not (1<=num<=8):
        num = int(input("Enter the size of board: "))
        print()
    # create a chess board
    game = Queens(num)
    # place the queens on the board
    game.solve()
    if game.solutions>1:
        print("There are",game.solutions,"solutions for a",num,"x",num,"board.")
    elif game.solutions == 1:
        print("There is", game.solutions, "solution for a", num, "x", num, "board.")
    else:
        print("There are no solutions for a", num, "x", num, "board.")


main()
